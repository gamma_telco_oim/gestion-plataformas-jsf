/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.views;

import com.gammapeit.resourcemanager.model.ResourceModel;
import com.gammapeit.resourcemanager.services.ResourceService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Data;

/**
 * @author Gammapeit
 */
@Data
@ManagedBean
@ViewScoped
public class ResourcesView implements Serializable {

    @ManagedProperty("#{resourceService}")
    private ResourceService service;

    private List<ResourceModel> resources;
    private List<ResourceModel> resourcesFiltered;
    private ResourceModel selectedResource;

    @PostConstruct
    public void init() {
        try {
            resources = service.getResources();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
