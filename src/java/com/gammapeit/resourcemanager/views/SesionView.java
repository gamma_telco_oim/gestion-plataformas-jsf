/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.views;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.gammapeit.resourcemanager.services.SeguridadService;
import com.gammapeit.resourcemanager.util.SesionUtil;
import com.gammapeit.resourcemanager.util.Utilities;

import lombok.Data;

/**
 * @author Gammapeit
 */
@Data
@ManagedBean
@ViewScoped
public class SesionView implements Serializable {

    @ManagedProperty("#{seguridadService}")
    private SeguridadService service;
    private Utilities p;

    private String usuario;
    private String password;

    @PostConstruct
    public void init() {
        try {
            p = Utilities.getInstance();
//			resources = service.getResources();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String login() throws Exception {
        Boolean autenticado = service.autenticar(usuario, password);
        String pagina = "recursos";
        if (autenticado) {
            HttpSession session = SesionUtil.getSession();
            session.setAttribute("username", usuario);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, p.getMessage("LOGIN_INVALIDO"), ""));
            pagina = null;
        }

        return pagina;
    }

    //logout event, invalidate session
    public String logout() {
        HttpSession session = SesionUtil.getSession();
        session.invalidate();
        return "login";
    }
}
