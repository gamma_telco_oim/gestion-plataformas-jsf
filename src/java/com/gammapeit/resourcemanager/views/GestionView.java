/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.views;

import com.gammapeit.resourcemanager.model.AdministratorModel;
import com.gammapeit.resourcemanager.model.UbicacionSuplenteModel;
import com.gammapeit.resourcemanager.services.GestionService;
import com.gammapeit.resourcemanager.util.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.extern.java.Log;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Gammapeit
 */
@Log
@Data
@ManagedBean
@ViewScoped
public class GestionView {

    private String resourceId;

    @ManagedProperty("#{gestionService}")
    private GestionService service;

    private DualListModel<String> ubicaciones;

    private HashMap<String, AdministratorModel> administradores;
    private DualListModel<AdministratorModel> substitutes;
    private List<String> perfiles;
    private List<String> opciones;

    private List<String> sysop_adm;
    private List<String> sysop_loc;

    private Utilities p;

    private String perfilSeleccionado;
    private String perfilNuevo;
    private String opcionSeleccionada;
    private String opcionNueva;

    private String resourceName;
    private String ubicacionAdmin;
    private String suplenteSel;
    private String adminSel;

    private AdministratorModel selectedAdmin;

    private List<UbicacionSuplenteModel> ubicacionesSuplentes = new ArrayList<UbicacionSuplenteModel>();
    private UbicacionSuplenteModel ubicacionSuplenteSeleccionado = new UbicacionSuplenteModel();

    HashMap<Object, Object> plataformasDueno;

    @PostConstruct
    public void init() {
        try {
            p = Utilities.getInstance();
            String dueno = service.getPlataformaDueno(resourceId);

            // GET RESOURCE ID
            HttpServletRequest parameters = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            resourceId = parameters.getParameter("r");

            // Locations
            List<String> locationSource = service.getAvailableLocations();
            List<String> locationTarget = service.getLocations(resourceId);
            ubicaciones = new DualListModel<String>(locationSource, locationTarget);

            // Get Substitutes
            ubicacionesSuplentes = service.getUbicacionesSuplentes(resourceId);

            // DD
            sysop_adm = new ArrayList<String>();
            sysop_loc = new ArrayList<String>();

            // Get current resource name
            resourceName = service.getLookupName(resourceId);

            // Get all available administrators
            administradores = service.getAdministrators();

            // Get current admin
            selectedAdmin = administradores.get(service.getPlataformaDueno(resourceId));

            // Profiles
            List<String> profilesTarget = service.getProfiles(resourceId);
            perfiles = profilesTarget;

            // Options
            List<String> optionsTarget = service.getOptions(resourceId);
            opciones = optionsTarget;

            // Substitutes
            List<AdministratorModel> supleSource = new ArrayList<AdministratorModel>(administradores.values());
            List<AdministratorModel> substitutesTarget = service.getPlataformaSuplentes(resourceId);
            substitutes = new DualListModel<AdministratorModel>(supleSource, substitutesTarget);
        } catch (Exception e) {
            log.severe(e.getMessage());
        }
    }

    // Buscar 
    public List<AdministratorModel> searchAdmin(String query) {
        List<AdministratorModel> result = new ArrayList<AdministratorModel>();
        for (AdministratorModel admin : administradores.values()) {
            if (admin.getName().toLowerCase().startsWith(query)) {
                result.add(admin);
            }
        }

        return result;
    }

    // Agregar perfil a la lista sin guardar datos
    public void addProfileToList() {
        if (!perfilNuevo.isEmpty()) {
            if (!perfiles.contains(perfilNuevo)) {
                perfiles.add(perfilNuevo);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format(p.getMessage("PROFILES_ITEM_ADDED"), perfilNuevo)));
                perfilNuevo = "";
            }
        }
    }

    // Eliminar perfil de la lista sin guardar datos
    public void deleteProfile() {
        if (perfiles.contains(perfilSeleccionado)) {
            perfiles.remove(perfilSeleccionado);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format(p.getMessage("PROFILES_ITEM_DELETED"), perfilSeleccionado)));
        }
    }

    // Agregar opción a la lista sin guardar datos
    public void addOptionToList() {
        if (!opcionNueva.isEmpty()) {
            if (!opciones.contains(opcionNueva)) {
                opciones.add(opcionNueva);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format(p.getMessage("OPTIONS_ITEM_ADDED"), opcionNueva)));
                opcionNueva = "";
            }
        }
    }

    // Eliminar opción de la lista sin guardar datos
    public void deleteOption() {
        if (opciones.contains(opcionSeleccionada)) {
            opciones.remove(opcionSeleccionada);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format(p.getMessage("OPTIONS_ITEM_DELETED"), opcionSeleccionada)));
        }
    }

    // Agregar ubicacionSuplente a la lista sin guardar datos
    public void addUbicacionSuplenteToList() {
        UbicacionSuplenteModel item = new UbicacionSuplenteModel(ubicacionAdmin, selectedAdmin.getCode(), suplenteSel);
        if (!ubicacionesSuplentes.contains(item) && item.getUbicacion() != null) {
            ubicacionesSuplentes.add(item);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("UBICACIONSUPLENTE_ITEM_ADDED")));
        }
    }

    // Eliminar ubicacion suplente de la lista sin guardar datos
    public void deleteUbicacionSuplente() {
        ubicacionesSuplentes.remove(ubicacionSuplenteSeleccionado);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format(p.getMessage("UBICACIONSUPLENTE_ITEM_DELETED"), opcionSeleccionada)));
    }

    // Selecciona el administrador
    public void addSelectedAdmin(SelectEvent event) {
        // Sin acción
    }

    // Get substitute name
    public String substituteName(String code) {
        return administradores.get(code).getName() != null ? administradores.get(code).getName() : code;
    }

    // Guardar todos los datos del formulario:::::
    public void save() {
        List<String> locs = ubicaciones.getTarget();

        // Actualizar Ubicaciones
        if (service.saveLocations(resourceId, locs)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("LOCATIONS_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("LOCATIONS_ERROR_SAVE")));
        }

        // Actualizar perfiles
        if (service.saveProfiles(resourceId, perfiles)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("PROFILES_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("PROFILES_ERROR_SAVE")));
        }

        // Actualizar opciones
        if (service.saveOptions(resourceId, opciones)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("OPTIONS_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("OPTIONS_ERROR_SAVE")));
        }

        // Actualiza nombre
        if (service.saveResourceName(resourceId, resourceName)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("NAME_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("NAME_ERROR_SAVE")));
        }

        // Actualiza administrador
        if (service.savePlataformaDueno(resourceId, selectedAdmin)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("ADMINISTRADORES_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("ADMINISTRADORES_ERROR_SAVE")));
        }

        // Actualiza suplentes
        if (service.savePlataformaSuplentes(resourceId, substitutes.getTarget())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("SUPLENTES_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("SUPLENTES_ERROR_SAVE")));
        }

        // Actualiza administrador por ubicación
        if (service.saveUbicacionesSuplentes(resourceId, ubicacionesSuplentes)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("UBICACIONSUPLENTE_UPDATED")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(p.getMessage("UBICACIONSUPLENTE_ERROR_SAVE")));
        }
    }
}
