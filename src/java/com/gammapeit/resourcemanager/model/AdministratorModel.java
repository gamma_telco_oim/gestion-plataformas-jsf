/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gammapeit
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdministratorModel {
    private String code;
    private String name;
    
    public String getAdmin(){
        if(name != null) {
            String[] users = name.split(",");
            return users[0];
        }
        
        return null;
    }
    
    public String getSubstitute(){
        if(name != null) {
            int i = name.indexOf(",");
            String user = null;
            if(i > 0){
                return name.substring(i);
            }
        }
        return null;
    }
}
