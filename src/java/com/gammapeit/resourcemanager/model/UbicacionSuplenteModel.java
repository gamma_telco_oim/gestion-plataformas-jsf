/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gammapeit
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UbicacionSuplenteModel {
    private String ubicacion;
    private String admin;
    private String suplente;
}
