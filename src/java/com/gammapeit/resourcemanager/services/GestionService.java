/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.services;

import com.gammapeit.resourcemanager.model.AdministratorModel;
import com.gammapeit.resourcemanager.model.UbicacionSuplenteModel;
import com.gammapeit.resourcemanager.util.LookupManager;
import com.gammapeit.resourcemanager.util.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import lombok.Data;

/**
 *
 * @author Gammapeit
 */
@Data
@ManagedBean(name = "gestionService")
@ApplicationScoped
public class GestionService {

    private final LookupManager manager;
    private final Utilities p;

    public GestionService() throws Exception {
        manager = new LookupManager();
        p = Utilities.getInstance();
    }

    /**
     * Obtener el nombre a mostrar del recurso seleccionado
     *
     * @param lokup id del lookup
     * @return devuelve un texto con el nombre del recurso
     * @throws Exception
     */
    public String getLookupName(String lokup) throws Exception {
        return manager.lookupDisplayName(lokup);
    }

    /**
     * Obtener lista de ubicaciones del lookup
     *
     * @param lookup nombre del lookup
     * @return devuelve un lista de valores con los nombres de la ubicaciones
     * @throws Exception
     */
    public List<String> getLocations(String lookup) throws Exception {
        List<String> l = new ArrayList<String>();
        HashMap locations = manager.findLookup(String.format(p.getProperty("LOOKUP_LOCATIONS"), lookup));
        for (Iterator it = locations.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Object, Object> item = (Map.Entry<Object, Object>) it.next();
            l.add(item.getValue().toString());
        }

        return l;
    }

    /**
     * Obtener lista de todas las ubicaciones posibles
     *
     * @return devuelve una lista de todas las ubicaciones
     * @throws Exception
     */
    public List<String> getAvailableLocations() throws Exception {
        List<String> l = new ArrayList<String>();
        HashMap locations = manager.findLookup(String.format("%1$s%2$s", p.getProperty("LOOKUP_RNC"), p.getProperty("RNC_UBICACIONES")));

        for (Iterator it = locations.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Object, Object> item = (Map.Entry<Object, Object>) it.next();
            l.add(item.getValue().toString());
        }

        return l;
    }

    /**
     * Actualización de las ubicaciones
     *
     * @param lookup nombre del lookup a actualizar
     * @param locations lista de ubicaciones finales para el lookup
     * @return
     */
    public boolean saveLocations(String lookup, List<String> locations) {
        Map values = new HashMap();

        for (String location : locations) {
            values.put(location, location);
        }

        return manager.updateLookup(String.format(p.getProperty("LOOKUP_LOCATIONS"), lookup), values);
    }

    /**
     * Guardar el nuevo nombre del recurso
     *
     * @param lookup id del lookup
     * @param newName nuevo nombre del recurso
     * @return
     */
    public boolean saveResourceName(String lookup, String newName) {
        return manager.updateLookupItemValue(p.getProperty("RNC_PLATAFORMAS"), lookup, lookup, newName);
    }

    /**
     * Obtener lista de administradores
     *
     * @return lista de administradores
     * @throws Exception
     */
    public HashMap<String, AdministratorModel> getAdministrators() throws Exception {
        HashMap<String, AdministratorModel> l = new HashMap<String, AdministratorModel>();
        HashMap admins = manager.findLookup(String.format(p.getProperty("LOOKUP_ADMINISTRADORES")));
        for (Iterator it = admins.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Object, Object> item = (Map.Entry<Object, Object>) it.next();
            l.put(item.getKey().toString(), new AdministratorModel(item.getKey().toString(), item.getValue().toString()));
        }
        return l;
    }

    /**
     * Obtiene el administrador de la plataforma
     *
     * @param lookup lookup code
     * @return adminitrador de la plataforma como un dueno
     * @throws Exception
     */
    public String getPlataformaDueno(String lookup) throws Exception {
        return manager.lookupDisplayItemValue(p.getProperty("LOOKUP_PLATAFORMA_DUENO"), lookup);
    }

    /**
     * Guardar el administrador de la plataforma
     *
     * @param lookup id del lookup
     * @param plataformaDueno dueño de la plataforma asociado con resourceID -
     * username
     * @return devuelve si la transacción fue exitosa
     */
    public boolean savePlataformaDueno(String lookup, AdministratorModel plataformaDueno) {
        return manager.updateLookupItemValue(p.getProperty("LOOKUP_PLATAFORMA_DUENO"), lookup, lookup, plataformaDueno.getCode());
    }

    /**
     * Obtener perfiles del lookup
     *
     * @param lookup id del lookup
     * @return devuelve una lista con los nombres de los perfiles
     * @throws Exception
     */
    public List<String> getProfiles(String lookup) throws Exception {
        List<String> l = new ArrayList<String>();
        HashMap profiles = manager.findLookup(String.format(p.getProperty("LOOKUP_PROFILES"), lookup));
        for (Iterator it = profiles.entrySet().iterator(); it.hasNext();) {
            HashMap.Entry<Object, Object> item = (HashMap.Entry<Object, Object>) it.next();
            l.add(item.getValue().toString());
        }

        return l;
    }

    /**
     * Guardar los perfiles configurados
     *
     * @param lookup id del lookup
     * @param profiles lista de perfiles finales del lookup
     * @return devuelve el resultado verdadero o falso de la operación
     */
    public boolean saveProfiles(String lookup, List<String> profiles) {
        Map values = new HashMap();

        for (String profile : profiles) {
            values.put(profile, profile);
        }

        return manager.updateLookup(String.format(p.getProperty("LOOKUP_PROFILES"), lookup), values);
    }

    /**
     * Guarda los suplentes de la plataforma
     *
     * @param lookup nombre del lookup
     * @param suplentes lista de suplentes
     * @return resultado de la operación
     */
    public boolean savePlataformaSuplentes(String lookup, List<AdministratorModel> suplentes) {
        Map values = new HashMap();

        for (AdministratorModel suplente : suplentes) {
            values.put(suplente.getCode(), suplente.getName());
        }

        return manager.updateLookup(String.format(p.getProperty("LOOKUP_SUPLENTES"), lookup), values);
    }

    /**
     * Obtener suplentes de la plataforma
     *
     * @param lookup nombre del lookup
     * @return lista de suplentes
     * @throws Exception
     */
    public List<AdministratorModel> getPlataformaSuplentes(String lookup) throws Exception {
        List<AdministratorModel> result = new ArrayList<AdministratorModel>();
        HashMap subs = manager.findLookup(String.format(p.getProperty("LOOKUP_SUPLENTES"), lookup));
        for (Iterator it = subs.entrySet().iterator(); it.hasNext();) {
            HashMap.Entry<Object, Object> item = (HashMap.Entry<Object, Object>) it.next();
            result.add(new AdministratorModel(item.getKey().toString(), item.getValue().toString()));
        }

        return result;
    }

    /**
     * Obtener opciones del lookup
     *
     * @param lookup id del lookup
     * @return devuelve una lista con los nombres de las opciones
     * @throws Exception
     */
    public List<String> getOptions(String lookup) throws Exception {
        List<String> l = new ArrayList<String>();
        HashMap options = manager.findLookup(String.format(p.getProperty("LOOKUP_OPCIONES"), lookup));
        for (Iterator it = options.entrySet().iterator(); it.hasNext();) {
            HashMap.Entry<Object, Object> item = (HashMap.Entry<Object, Object>) it.next();
            l.add(item.getValue().toString());
        }

        return l;
    }

    /**
     * Guardar las opciones configuradas
     *
     * @param lookup id del lookup
     * @param options lista de opciones finales del lookup
     * @return devuelve el resultado verdadero o falso de la operación
     */
    public boolean saveOptions(String lookup, List<String> options) {
        Map values = new HashMap();

        for (String option : options) {
            values.put(option, option);
        }

        return manager.updateLookup(String.format(p.getProperty("LOOKUP_OPCIONES"), lookup), values);
    }

    /**
     * Obtener ubicaciones suplente del lookup
     *
     * @param lookup id del lookup
     * @return devuelve una lista con las ubicaciones suplente
     * @throws Exception
     */
    public List<UbicacionSuplenteModel> getUbicacionesSuplentes(String lookup) throws Exception {
        List<UbicacionSuplenteModel> l = new ArrayList<UbicacionSuplenteModel>();
        HashMap ubicacionesSuplentes = manager.findLookup(String.format(p.getProperty("LOOKUP_UBICACION_SUPLENTE"), lookup));
        for (Iterator it = ubicacionesSuplentes.entrySet().iterator(); it.hasNext();) {
            HashMap.Entry<Object, Object> item = (HashMap.Entry<Object, Object>) it.next();
            String[] valor = item.getValue().toString().split(",");
            String admin = valor[0];
            String suplentes = valor[1];

            l.add(new UbicacionSuplenteModel(item.getKey().toString().split("-")[0], admin, suplentes));
        }

        return l;
    }

    /**
     * Guardar las ubicaciones suplente configuradas
     *
     * @param lookup id del lookup
     * @param ubicacionesSuplentes
     * @return devuelve el resultado verdadero o falso de la operación
     */
    public boolean saveUbicacionesSuplentes(String lookup, List<UbicacionSuplenteModel> ubicacionesSuplentes) {
        Map values = new HashMap();

        for (UbicacionSuplenteModel us : ubicacionesSuplentes) {
            values.put(String.format("%1$s-%2$s", us.getUbicacion(), us.getSuplente()), String.format("%1$s,%2$s", us.getAdmin(), us.getSuplente()));
        }

        return manager.updateLookup(String.format(p.getProperty("LOOKUP_UBICACION_SUPLENTE"), lookup), values);
    }
}
