/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.services;

import com.gammapeit.resourcemanager.model.ResourceModel;
import com.gammapeit.resourcemanager.util.LookupManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.security.auth.login.LoginException;

/**
 * Faces Service
 *
 * @author Gammapeit
 */
@ManagedBean(name = "resourceService")
@ApplicationScoped
public class ResourceService {

    private final LookupManager manager;
    private final String RES_LOOKUP = "Lookup.RNC.Plataformas";

    public ResourceService() throws LoginException {
        manager = new LookupManager();
    }

    /**
     * Obtiene una lista con todos los recursos modificables
     * 
     * @return devuelve una lista de recursos
     * @throws Exception 
     */
    public List<ResourceModel> getResources() throws Exception {
        List<ResourceModel> resources = new ArrayList<ResourceModel>();

        HashMap tr = manager.findLookup(RES_LOOKUP);
        
        for (Iterator it = tr.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Object, Object> item = (Map.Entry<Object, Object>) it.next();
            resources.add(new ResourceModel(item.getKey().toString(), item.getValue().toString()));
        }

        return resources;
    }
}
