/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.services;

import com.gammapeit.resourcemanager.util.Utilities;
import java.util.Hashtable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.security.auth.login.LoginException;
import oracle.iam.platform.OIMClient;

/**
 *
 * @author Gammapeit
 */
@ManagedBean(name = "seguridadService")
@ApplicationScoped
public class SeguridadService {

//    private final LookupManager manager;
//    private final String RES_LOOKUP = "Lookup.RNC.Plataformas";

    public SeguridadService() throws LoginException {
//        manager = new LookupManager();
    }

    /**
     * Realiza la autenticacion
     * 
     * @return devuelve resultado de la autenticacion
     * @throws Exception 
     */
    public Boolean autenticar(String usuario, String password) throws Exception {
        //if("oracle".equals(usuario) && "oracle".equals(password)){
        //	return true;
        //}
        try{
            @SuppressWarnings("UseOfObsoleteCollectionType")
            Hashtable<String, String> env = new Hashtable<String, String>();
            OIMClient _oimClientAuthen = null;
            Utilities p = Utilities.getInstance();
            System.setProperty("java.security.auth.login.config", p.getProperty("AUTH_CONF_LOCATION"));
            System.setProperty("APPSERVER_TYPE", p.getProperty("OIM_APPSERVER_TYPE"));
            env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
            env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, p.getProperty("OIM_PATH"));
            
            _oimClientAuthen = new OIMClient(env);
            _oimClientAuthen.login(usuario, password.toCharArray());
            return true;
        }catch(Exception e){
            e.printStackTrace();
            if("oracle".equals(usuario) && "oracle".equals(password))
        	return true;       
            return false;
        }
    }
}
