/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.util;

import com.gammapeit.resourcemanager.model.AdministratorModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Gammapeit
 */
public class Utilities {

    private Utilities() {
    }

    public static Utilities getInstance() {
        return new Utilities();
    }

    /**
     * Obtener valor de una propiedad
     *
     * @param key identificador de la propiedad
     * @return devuelve el valor de la propiedad
     */
    public String getProperty(String key) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            Properties p = new Properties();
            p.load(ec.getResourceAsStream("/WEB-INF/settings.properties"));

            return p.getProperty(key);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Obtener valor de un mensaje
     *
     * @param key identificador de la propiedad
     * @return devuelve el valor de la propiedad
     */
    public String getMessage(String key) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            Properties p = new Properties();
            p.load(ec.getResourceAsStream("/WEB-INF/messages.properties"));

            return p.getProperty(key);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
