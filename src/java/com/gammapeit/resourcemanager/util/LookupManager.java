/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.util;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcDuplicateLookupCodeException;
import Thor.API.Exceptions.tcInvalidAttributeException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcInvalidValueException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import lombok.Data;
import lombok.extern.java.Log;
import oracle.iam.catalog.api.CatalogService;
import oracle.iam.conf.api.SystemConfigurationService;
import oracle.iam.configservice.api.Constants;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ApplicationInstanceService;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.request.api.RequestService;

/**
 *
 * @author Gammapeit
 */
@Log
@Data
public class LookupManager {

    protected OIMClient _oimClientAuthen = null;
    protected OrganizationManager omgr;
    protected UserManager umgr;
    protected CatalogService csrv;
    protected RequestService rsrv;
    private ApplicationInstanceService aisrv;
    private ProvisioningService psrv;
    private NotificationService nsrv;
    private SystemConfigurationService scsrv;

    private Utilities p;

    public LookupManager() throws LoginException {
        @SuppressWarnings("UseOfObsoleteCollectionType")
        Hashtable<String, String> env = new Hashtable<String, String>();
        p = Utilities.getInstance();

        System.setProperty("java.security.auth.login.config", p.getProperty("AUTH_CONF_LOCATION"));
        System.setProperty("APPSERVER_TYPE", p.getProperty("OIM_APPSERVER_TYPE"));
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, p.getProperty("OIM_PATH"));
        _oimClientAuthen = new OIMClient(env);
        _oimClientAuthen.login(p.getProperty("OIM_USERNAME"), p.getProperty("OIM_PASSWORD").toCharArray());

        umgr = _oimClientAuthen.getService(UserManager.class);
        omgr = _oimClientAuthen.getService(OrganizationManager.class);
        csrv = _oimClientAuthen.getService(CatalogService.class);
        rsrv = _oimClientAuthen.getService(RequestService.class);
        aisrv = _oimClientAuthen.getService(ApplicationInstanceService.class);
        psrv = _oimClientAuthen.getService(ProvisioningService.class);
        nsrv = _oimClientAuthen.getService(NotificationService.class);
        scsrv = _oimClientAuthen.getService(SystemConfigurationService.class);
    }

    public void logout() {
        _oimClientAuthen.logout();
    }

    /**
     * Elimina el prefijo antes del caracter ~ del Lookup del parámetro
     *
     * @param pLookupName
     * @throws Exception
     */
    public void fixLookups(String pLookupName) throws Exception {
        log.log(Level.INFO, "iniciando fixLookups {0}", pLookupName);
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                log.log(Level.INFO, "iniciando fixLookups {0}", lookupDetValue);
                if (lookupDetValue.contains("~")) {
                    int initValor = lookupDetValue.indexOf("~") + 1;
                    int finValor = lookupDetValue.length();
                    String nostart = lookupDetValue.substring(initValor, finValor);
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", nostart);

                    log.log(Level.INFO, "actualizando {0}/{1} -------- {2}", new Object[]{lookupCode, lookupDetValue, nostart});

                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                }
            }
        }
    }

    /**
     * Encuentra el listado del Lookups del OIM y agrega el prefijo del id
     *
     * @param LookupName
     * @throws Exception
     */
    public void addPrefixLookups(String LookupName) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", LookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                if (lookupDetCode.contains("~")) {
                    int initValor = lookupDetCode.indexOf("~") + 1;
                    int finValor = lookupDetCode.length();
                    String nostart = lookupDetCode.substring(initValor, finValor);
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", nostart + " - " + lookupDetValue);
                    log.log(Level.INFO, "actualizando {0}/{1} -------- {2} - {3}", new Object[]{lookupCode, lookupDetValue, nostart, lookupDetValue});
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                } else {
                    break;
                }
            }
        }
    }

    /**
     * Encuentra el listado del Lookups del OIM para y agrega el sufijo con el
     * nombre del Looukup Padre
     *
     * @param LookupName
     * @param idlookup
     * @param sufix
     * @throws Exception
     */
    public void addSufixLookups(String LookupName, String idlookup, String sufix) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", LookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");

            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                if (idlookup.equals(lookupDetCode) && !lookupDetValue.contains(sufix)) {
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue + " [" + sufix + "]");
                    System.out.println("actualizando " + lookupCode + "/" + lookupDetValue + "-" + sufix);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                    break;
                }
            }
        }
    }

    /**
     * Encuentra el listado del Lookups del OIM para y remueve el primer indice
     *
     * @param pLookupName
     * @param sufix
     * @throws Exception
     */
    public void removeSufixLookups(String pLookupName, String sufix) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);

        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    //System.out.println(lookupDetCode + "\t" + lookupDetValue );
                    int init = lookupDetValue.indexOf("-") + 1;
                    int end = lookupDetValue.length();

                    //System.out.println(" ... " +  );
                    lookupDetValue = lookupDetValue.substring(init, end).trim();

                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                    System.out.println("removiendo/actualizando " + lookupCode + "/" + lookupDetValue);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                }
            }
        }
    }

    /**
     * Encuentra el Lookups
     *
     * @param pLookupName
     * @throws Exception
     */
    public void findLookups(String pLookupName) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);

        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    log.log(Level.INFO, "{0}\t{1}", new Object[]{lookupDetCode, lookupDetValue});
                }
            }
        }
        log.info("\n\n*********************************************************************************************************************");
    }

    /**
     * Actualizar el código y el valor de un item específico dentro del lookup
     *
     * @param lookup id del lookup a modificar
     * @param item "key" del item a modificar dentro del lookup
     * @param key nuevo "key" que tendrá el item
     * @param newValue nuevo "value" que tendrá el item del lookup
     * @return devuelve si la operación fue exitosa o erronea
     */
    public boolean updateLookupItemValue(String lookup, String item, String key, String newValue) {
        try {
            tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);

            HashMap<String, String> values = new HashMap<String, String>();
            values.put(Constants.TableColumns.LKV_ENCODED.toString(), key);
            values.put(Constants.TableColumns.LKV_DECODED.toString(), newValue);
    
            if (lookupDisplayItemValue(lookup, key).isEmpty()) {
                loi.addLookupValue(lookup, key, newValue, "", "");
            } else {
                loi.updateLookupValue(lookup, item, values);
            }

            return true;
        } catch (tcInvalidAttributeException ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (tcAPIException ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (tcInvalidLookupException ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (tcInvalidValueException ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    /**
     * Obtener el valor de un item específico dentro de un lookup
     *
     * @param lookupName "key" del item a encontrar en el lookup de plataformas
     * @return devuelve un texto con el nombre del recurso
     * @throws Exception
     */
    public String lookupDisplayName(String lookupName) throws Exception {
        tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", p.getProperty("RNC_PLATAFORMAS"));
        String response = "";

        tcResultSet rs = loi.findLookupsDetails(params);

        for (int i = 0; i < rs.getRowCount(); i++) {
            rs.goToRow(i);

            String lookupCode = rs.getStringValue("Lookup Definition.Code");
            String lookupType = rs.getStringValue("Lookup Definition.Type");

            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = loi.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());

                    if (lookupDetCode.equals(lookupName)) {
                        response = lookupDetValue;
                    }
                }
            }
        }

        return response;
    }

    /**
     * Obtener el valor de un item específico dentro de un lookup
     *
     * @param lookup Lookup
     * @param key lookup item key
     * @return devuelve un texto con el nombre del recurso
     * @throws Exception
     */
    public String lookupDisplayItemValue(String lookup, String key) throws Exception {
        tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", lookup);
        String response = "";

        tcResultSet rs = loi.findLookupsDetails(params);

        for (int i = 0; i < rs.getRowCount(); i++) {
            rs.goToRow(i);

            String lookupCode = rs.getStringValue("Lookup Definition.Code");
            String lookupType = rs.getStringValue("Lookup Definition.Type");

            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = loi.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());

                    if (lookupDetCode.equals(key)) {
                        response = lookupDetValue;
                    }
                }
            }
        }

        return response;
    }

    /**
     * Encuentra el Lookup y devuelve el resultado
     *
     * @param lookupName
     * @return
     * @throws Exception
     */
    public HashMap<Object, Object> findLookup(String lookupName) throws Exception {
        tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", lookupName);
        HashMap<Object, Object> response = new HashMap<Object, Object>();

        tcResultSet rs = loi.findLookupsDetails(params);

        for (int i = 0; i < rs.getRowCount(); i++) {
            rs.goToRow(i);

            String lookupCode = rs.getStringValue("Lookup Definition.Code");
            String lookupType = rs.getStringValue("Lookup Definition.Type");

            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = loi.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());

                    response.put(lookupDetCode, lookupDetValue);
                }
            }
        }

        return response;
    }

    /**
     * Crea un Lookup Nuevo
     *
     * @param lookupName nombre del lookup
     * @return
     */
    public boolean createLookup(String lookupName) {
        try {
            tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
            loi.addLookupCode(lookupName);

            return true;
        } catch (tcAPIException ex) {
            log.log(Level.SEVERE, null, ex);
        } catch (tcDuplicateLookupCodeException ex) {
            log.fine("Lookup Existente");
            return true;
        }

        return false;
    }

    /**
     * Actualiza los datos del lookup,
     *
     * 1. Primero revisa los valores existentes. 2. Compara con los valores
     * definitivos. 3. Agrega los valores existentes no definitivos a una lista
     * de eliminación. 4. Agrega los valores inexistentes definitivos a una
     * lista de creación. 5. Elimina los datos según la lista de eliminación. 6.
     * Crea los datos según la lista de creación.
     *
     * @param lookupName nombre del lookup
     * @param values valores definitivos
     * @return devuelve un verdadero o falso al final del proceso
     */
    public boolean updateLookup(String lookupName, Map<String, String> values) {
        try {
            tcLookupOperationsIntf loi = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
            Map params = new HashMap();
            params.put("Lookup Definition.Code", lookupName);
            tcResultSet rs = loi.findLookupsDetails(params);

            List<String> removeList;
            removeList = new ArrayList<String>();

            for (int i = 0; i < rs.getRowCount(); i++) {
                rs.goToRow(i);

                String lookupCode = rs.getStringValue("Lookup Definition.Code");
                String lookupType = rs.getStringValue("Lookup Definition.Type");

                if (lookupType.contains("l")) {
                    tcResultSet trs = loi.getLookupValues(lookupCode);

                    for (int j = 0; j < trs.getRowCount(); j++) {
                        trs.goToRow(j);

                        String lookupDetCode = trs.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());

                        if (values.containsKey(lookupDetCode)) {
                            values.remove(lookupDetCode);
                        } else {
                            removeList.add(lookupDetCode);
                        }
                    }
                }
            }

            // DELETE
            if (removeList.size() > 0) {
                for (String item : removeList) {
                    loi.removeLookupValue(lookupName, item);
                }
            }

            // CREATE
            if (values.size() > 0) {
                for (Iterator it = values.entrySet().iterator(); it.hasNext();) {
                    HashMap.Entry<Object, Object> object = (HashMap.Entry<Object, Object>) it.next();

                    loi.addLookupValue(lookupName, object.getKey().toString(), object.getValue().toString(), "", "");
                }
            }

            return true;
        } catch (tcAPIException ex) {
            log.severe(ex.getMessage());
        } catch (tcColumnNotFoundException ex) {
            log.severe(ex.getMessage());
        } catch (tcInvalidAttributeException ex) {
            log.severe(ex.getMessage());
        } catch (tcInvalidLookupException ex) {
            if (createLookup(lookupName)) {
                return updateLookup(lookupName, values);
            }

            log.severe(ex.getMessage());
        } catch (tcInvalidValueException ex) {
            log.severe(ex.getMessage());
        }
        
        return false;
    }

    /**
     * Mueve los datos del lookup origen al lookup destino
     *
     * @param pLookupOld
     * @param pLookupNew
     * @throws Exception
     */
    private void copyLookups(String pLookupOld, String pLookupNew) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupOld);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(pLookupOld);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                Map<String, String> updateMap = new HashMap<String, String>();
                updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                log.log(Level.INFO, "agregando {0}/{1} -------- {2}", new Object[]{pLookupNew, lookupDetValue, lookupDetValue});
                try {
                    lookupOperationsIntf.addLookupValue(pLookupNew, lookupDetCode, lookupDetValue, "", "");
                } catch (tcAPIException e) {
                    log.log(Level.SEVERE, "Fallo en {0} - {1}", new Object[]{lookupDetCode, lookupDetValue});
                } catch (tcInvalidLookupException e) {
                    log.log(Level.SEVERE, "Fallo en {0} - {1}", new Object[]{lookupDetCode, lookupDetValue});
                } catch (tcInvalidValueException e) {
                    System.err.println("Fallo en " + lookupDetCode + " - " + lookupDetValue);
                }
            }
        }
    }

    /**
     *
     * @param pLookup
     * @throws Exception
     */
    private void removeLookups(String pLookup) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookup);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(pLookup);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                Map<String, String> updateMap = new HashMap<String, String>();
                updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                System.out.println(" ... checking ... " + lookupDetCode);
                System.out.println("Eliminando " + pLookup + "/" + lookupDetValue + " -------- " + lookupDetValue);
                lookupOperationsIntf.removeLookupValue(pLookup, lookupDetCode);
            }
        }
    }
}
