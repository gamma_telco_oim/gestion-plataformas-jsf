/*
 * ResourceManager was developed by Gammapeit SAS
 * 
 * @name                ResourceManager
 * @version             1.2
 * @author              Jose Buelvas Santos
 * @copyright           Gammapeit SAS (c) 2016
 * @companyURL          https://www.gammapeit.com
 */
package com.gammapeit.resourcemanager.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Gammapeit
 */
public class SesionUtil {

	private SesionUtil() {
	}

	public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public static String getUserName() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return session.getAttribute("username").toString();
	}

	public static String getUserId() {
		HttpSession session = getSession();
		if (session != null)
			return (String) session.getAttribute("userid");
		else
			return null;
	}
}
